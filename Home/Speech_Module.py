FIRST_TEN = ["one", "two", "three", "four", "five", "six", "seven",
             "eight", "nine"]
SECOND_TEN = ["ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen",
              "sixteen", "seventeen", "eighteen", "nineteen"]
OTHER_TENS = ["twenty", "thirty", "forty", "fifty", "sixty", "seventy",
              "eighty", "ninety"]
HUNDRED = "hundred"


def checkio(number):
    quot = number // 100
    rest = number % 100
    if quot > 0:
        return format("%s %s %s" % (FIRST_TEN[quot - 1], HUNDRED, checkio(rest))).strip()

    quot = number // 10
    rest = number % 10
    if quot == 1:
        return SECOND_TEN[rest]
    elif quot > 1:
        return format("%s %s" % (OTHER_TENS[quot - 2], checkio(rest))).strip()

    if number > 0:
        return FIRST_TEN[number - 1]

    #replace this for solution
    return ''


#These "asserts" using only for self-checking and not necessary for auto-testing
if __name__ == '__main__':
    assert checkio(4) == 'four', "1st example"
    assert checkio(133) == 'one hundred thirty three', "2nd example"
    assert checkio(12) == 'twelve', "3rd example"
    assert checkio(101) == 'one hundred one', "4th example"
    assert checkio(212) == 'two hundred twelve', "5th example"
    assert checkio(40) == 'forty', "6th example"
    assert not checkio(212).endswith(' '), "Don't forget strip whitespaces at the end of string"
