def checkio(number):

    i = 0
    ret = 0
    total = 0
    while number > 0:
        i += 1
        total += i
        if number >= total:
            ret = total
        number -= total
        if number >= total:
            ret = number

    return ret

#These "asserts" using only for self-checking and not necessary for auto-testing
if __name__ == '__main__':
    assert checkio(1) == 1, "1st example"
    assert checkio(2) == 1, "2nd example"
    assert checkio(5) == 3, "3rd example"
    assert checkio(10) == 6, "4th example"
