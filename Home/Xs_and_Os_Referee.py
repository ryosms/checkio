def checkio(game_result):
    for idx in range(0, 3):
        if game_result[idx][0] != "." and game_result[idx][0] == game_result[idx][1] and game_result[idx][0] == game_result[idx][2]:
            return game_result[idx][0]
        if game_result[0][idx] != "." and game_result[0][idx] == game_result[1][idx] and game_result[0][idx] == game_result[2][idx]:
            return game_result[0][idx]

    if game_result[0][0] != "." and game_result[0][0] == game_result[1][1] and game_result[0][0] == game_result[2][2]:
        return game_result[0][0]
    if game_result[0][2] != "." and game_result[0][2] == game_result[1][1] and game_result[0][2] == game_result[2][0]:
        return game_result[0][2]

    return "D"

#These "asserts" using only for self-checking and not necessary for auto-testing
if __name__ == '__main__':
    assert checkio([
        "X.O",
        "XX.",
        "XOO"]) == "X", "Xs wins"
    assert checkio([
        "OO.",
        "XOX",
        "XOX"]) == "O", "Os wins"
    assert checkio([
        "OOX",
        "XXO",
        "OXX"]) == "D", "Draw"
    assert checkio([
        "O.X",
        "XX.",
        "XOO"]) == "X", "Xs wins again"

