def checkio(text):

    #replace this for solution
    max_freq = 0
    ret = ''
    lower_text = text.lower()
    for ch in lower_text:
        if not ch.isalpha():
            continue
        freq = lower_text.count(ch)
        if freq > max_freq or (freq == max_freq and ch < ret):
            max_freq = freq
            ret = ch

    return ret

#These "asserts" using only for self-checking and not necessary for auto-testing
if __name__ == '__main__':
    assert checkio("Hello World!") == "l", "Hello test"
    assert checkio("How do you do?") == "o", "O is most wanted"
    assert checkio("One") == "e", "All letter only once."
    assert checkio("Oops!") == "o", "Don't forget about lower case."
    assert checkio("AAaooo!!!!") == "a", "Only letters."
