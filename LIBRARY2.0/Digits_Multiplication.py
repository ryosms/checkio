def checkio(number):
    chars = str(number).replace("0", "")
    if len(chars) == 0:
        return 0

    multi = 1
    for ch in chars:
        multi *= int(ch)

    return multi

#These "asserts" using only for self-checking and not necessary for auto-testing
if __name__ == '__main__':
    assert checkio(123405) == 120
    assert checkio(999) == 729
    assert checkio(1000) == 1
    assert checkio(1111) == 1
